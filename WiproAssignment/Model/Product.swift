//
//  Product.swift
//  WiproAssignment
//
//  Created by Harsh Singh
//  Copyright © 2019 Harsh Singh. All rights reserved.
//

import UIKit

/// Product Model class
class Product: NSObject {
    
/// Product data member
    struct ProductData {
        var title: String
        var description: String
        var imageUrl: String
        
        init(_ dictionary: [String: Any]) {
            self.title = dictionary[Constant.ApiKeys.title] as? String ?? ""
            self.description = dictionary[Constant.ApiKeys.description] as? String ?? ""
            self.imageUrl = dictionary[Constant.ApiKeys.imageUrl] as? String ?? ""
        }
    }
    
}
