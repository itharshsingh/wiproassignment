//
//  RootViewModel.swift
//  WiproAssignment
//
//  Created by Harsh Singh
//  Copyright © 2019 Harsh Singh. All rights reserved.
//

import UIKit

protocol RootViewModelDelegate {
    func rootViewModelRecieveData(_ infoData: [Product.ProductData], _ navigattionTitle: String, _ error: String)
}

class RootViewModel: NSObject {
    
    var delegate: RootViewModelDelegate?
    
    // MARK: - Api call for geting data
    func getImagesData(apiUrl: String) -> Void{
        ServiceManager.sharedInstance.getallDataFromApi(contenturl:apiUrl, getRequestCompleted: { [weak self] (responseData, responseCode) in
            
            if responseCode == Constant.ApiResponseCode.Success{
                if let responseDict = responseData[Constant.ApiKeys.rows] as? [[String : AnyObject]]{
                    var model = [Product.ProductData]()
                    model = responseDict.compactMap{ (dictionary) in
                        return Product.ProductData(dictionary)
                    }
                    let navTitle = responseData[Constant.ApiKeys.title] as? String
                    self?.delegate?.rootViewModelRecieveData(model,navTitle ?? Constant.DefaultValue.text, Constant.DefaultValue.text )
                }
            }
            else{
                self?.delegate?.rootViewModelRecieveData([Product.ProductData](),Constant.DefaultValue.text ,Constant.ErrorMessage.messageText)
            }
        })
    }
}
