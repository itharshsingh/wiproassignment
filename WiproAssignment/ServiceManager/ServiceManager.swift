//
//  ServiceManager.swift
//  WiproAssignment
//
//  Created by Harsh Singh
//  Copyright © 2019 Harsh Singh. All rights reserved.
//

import UIKit
let sharedSession = URLSession.shared

/// Service manager class
class ServiceManager: NSObject {
    
    /// shared Instance
    static let sharedInstance = ServiceManager()
    
    // MARK :- Private Method

    private override init() {
        super.init()
    }
    
    // MARK :- Public Method
    
    /**
     This method create get url request
     
     - parameter url: it`s a base url.
     - returns: It`s retuen a nsmutable url request.
     **/
    func CreateGetUrlRequest(_ url: String) -> NSMutableURLRequest{
        let urlString = url
        if let url = URL(string: urlString){
            let header = ["Content-Type": "text/plain"]
            let urlRequest = NSMutableURLRequest(url: url as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 360)
            urlRequest.httpMethod = "Get"
            urlRequest.allHTTPHeaderFields = header
            return urlRequest
        }
        return NSMutableURLRequest()
    }
    
    /**
    This method get all data from api
    
    - parameter contenturl: it`s a base url.
    - parameter getRequestCompleted: It`s a complementation handler block. That`s featch a json dictnary.
    - parameter msg: it`s a integer value
     **/
    func getallDataFromApi(contenturl: String, getRequestCompleted : @escaping ( _ jsonDict:[String: AnyObject],  _ msg: NSInteger) -> ()) {
        let urlRequest = CreateGetUrlRequest(contenturl)
        let task = sharedSession.dataTask(with: urlRequest as URLRequest, completionHandler: { (data, response, error) in
            if error != nil{
                //print("Error description\(String(describing: error?.localizedDescription))")
            }else{
                let httpResponse = response as! HTTPURLResponse
                
                if let dataExist = data{
                    let responseData: Any?
                    do{
                        if httpResponse.statusCode == Constant.ApiResponseCode.Success {
                            let responseStrInISOLatin = String(data:dataExist, encoding: String.Encoding.isoLatin1)
                            guard let modifiedDataInUTF8Format = responseStrInISOLatin?.data(using: String.Encoding.utf8) else {
                                return
                            }
                            responseData = try JSONSerialization.jsonObject(with: modifiedDataInUTF8Format, options: .allowFragments)
                            if let responseDictData = responseData as? [String : AnyObject]{
                                getRequestCompleted(responseDictData, httpResponse.statusCode)
                            }
                        }else{
                            getRequestCompleted([:], httpResponse.statusCode)
                        }
                    }catch{
                        responseData = nil
                    }
                }
            }
        })
        task.resume()
    }
}
